#!/bin/bash

# variables
cp_api_url="https://10.0.102.100"
api_user="api_user"
api_pw="cpx2017"
policy_package="standard"
targets="vsec-gw"

#Login and retrieve the session_id
SID=`curl -k -H "Content-Type: application/json" -H "Accept: bla" -X POST -d '{"user":"'"$api_user"'","password":"'"$api_pw"'"}' $cp_api_url/web_api/login | ./jq-linux64 '.sid' | sed s/\"//g`


#install policy

curl -k \
-H "Content-Type: application/json" \
-H "Accept: bla" \
-H "X-chkp-sid: $SID" \
-X POST -d '{"policy-package":"'"$policy_package"'","access":"true","threat-prevention":"false","targets":["'"$targets"'"]}' \
$cp_api_url/web_api/install-policy


#logout
curl -k -H "Content-Type: application/json" -H "Accept: bla" -H "X-chkp-sid: $SID" -X POST -d '{}' $cp_api_url/web_api/logout
