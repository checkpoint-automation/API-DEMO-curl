#!/bin/bash

# variables
cp_api_url="https://10.0.0.254"
api_user="api_user"
api_pw="vpn123"
policy_name="DEMO-POLICY"
threat_prevention="false"
access="true"

##clear
clear

#curl or curl_cli
if [ ! $(which curl >/dev/null 2>&1) ] ; then
        alias curl=curl_cli
        shopt -s expand_aliases
fi

#jq is required
if [ $(which jq >/dev/null 2>&1) ] ; then
    echo "jq is required for this script to run"
    exit
fi

#Login and retrieve the session_id
SID=`curl -sk \
-H "Content-Type: application/json" \
-H "Accept: bla" \
-X POST -d '{"user":"'"api_user"'","password":"'"$api_pw"'"}' \
$cp_api_url/web_api/login | jq '.sid' | sed s/\"//g`
echo ""
echo "#### LOGIN ACTION: ####"
echo "# SESSION-ID = "$SID

#deleting the policy
echo ""
echo "#### DELETE POLICY ####"
curl -sk \
-H "Content-Type: application/json" \
-H "Accept: bla" \
-H "X-chkp-sid: $SID" \
-X POST -d '{"name":"'"$policy_name"'"}' \
$cp_api_url/web_api/delete-package


#publish
echo ""
echo "#### PUBLISH ACTION ####"
task_id=`curl -sk \
-H "Content-Type: application/json" \
-H "Accept: bla" \
-H "X-chkp-sid: $SID" \
-X POST -d '{}' \
$cp_api_url/web_api/publish | jq --raw-output '."task-id"'`

# wait for task to finish
echo "# TASK-ID: "$task_id

task_status=`curl -sk \
-H "Content-Type: application/json" \
-H "Accept: bla" \
-H "X-chkp-sid: $SID" \
-X POST -d '{"task-id":"'"$task_id"'"}' \
$cp_api_url/web_api/show-task | jq --raw-output '.tasks[].status'`
while [ "$task_status" == "in progress" ]
do
        task_status=`curl -sk \
        -H "Content-Type: application/json" \
        -H "Accept: bla" \
        -H "X-chkp-sid: $SID" \
        -X POST -d '{"task-id":"'"$task_id"'"}' \
        $cp_api_url/web_api/show-task | jq --raw-output '.tasks[].status'`
        echo "# TASK-STATUS: " $task_status
done

#logout
echo ""
echo "#### LOGOUT ACTION ####"
curl -sk -H "Content-Type: application/json" \
-H "Accept: bla" \
-H "X-chkp-sid: $SID" \
-X POST -d '{}' \
$cp_api_url/web_api/logout | jq '.message'
